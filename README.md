# Hornero: Zorzal Servidor

## Prerequisitos
descargar Inteprete de javascript [Nodejs](https://nodejs.org/en/) luego ejecutar:

```java
npm install -g typescript //compilador typescript
```

## Intalacion de base de datos
La base de datos que se uso es SQL, puntualmente se usa mariaDB. El archivo de importacion se encuentra en `./Hornero_zorzal.sql`. 

## Instalacion del server
ejecutar 

```bash
cd ./honero-server
npm install
```

## Ejecutar server de desarrollo

Ejecutar `npm run dep` esto compila el programa fuente de la carpeta `src`  en la carpeta `build` 

### Configuracion Base de datos
Para configurar el acceso a la base de datos cambiar el archivo `ormconfig.json`.

## Compilacion
Para compilar ejecutar `npm run build`

