import {  Socket, Server } from "socket.io";
import { verify } from "jsonwebtoken";
import { chatInterface,usuarioSala,EVENTO_CHAT } from "./model/interfaces";
import { key } from "../env";
import { Sala } from "./model/Sala";

export const chatHandler = (socket: Socket,io:Server,salas:any) => {

    socket.on(EVENTO_CHAT.enviarMensaje ,(data:chatInterface) =>{
        let payload = verify(data.usuario, key);
        const usuario:usuarioSala = payload["usuario"];
 
        const sala: Sala = salas[data.token];
        if (sala) {
          if (!sala.existeUsuario(usuario.nombreUsuario)) {
            sala.agregarUsuario(usuario.nombreUsuario, socket.id);
            io.to(sala.tokenSala).emit("getUsuarios", sala.usuariosConectados);
          }
          sala.chat.agregarMensaje(data.mensaje);
          socket.to(data.token).emit(EVENTO_CHAT.recibirMensaje,data.mensaje);
        }
  
      })
}