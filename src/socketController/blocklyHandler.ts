import { Server, Socket } from "socket.io";
import { verify } from "jsonwebtoken";
import { key } from "../env";
import { blockHandler, marcaControler, usuarioSala, chatInterface } from "./model/interfaces";
import { Sala } from "./model/Sala";

export const blocklyHandler = (socket: Socket,io:Server,salas) => {

    socket.on("conexionBloques", (data: blockHandler) => {
      console.log(data);
      let payload;
      try {
        payload = verify(data.usuario, key);
      } catch (error) {
        return;
      }
      const usuario = payload["usuario"];
      socket.join(data.token);
      if (!salas[data.token]) {
        const nuevaSala = new Sala(data.token);
        const userSala = nuevaSala.agregarUsuario(
          usuario.nombreUsuario,
          socket.id
        );
        salas[data.token] = nuevaSala;
        socket.emit("getColor", userSala.color);
        io.to(nuevaSala.tokenSala).emit(
          "getUsuarios",
          nuevaSala.usuariosConectados
        );
      } else {
        const sala: Sala = salas[data.token];
        if (!sala.existeUsuario(usuario.nombreUsuario))
          sala.agregarUsuario(usuario.nombreUsuario, socket.id);
        const mensaje = {
          token: data.token,
          xml: sala.xml,
          usuariosConectados: sala.usuariosConectados,
        };
        //envio color del jugador
        socket.emit("getColor", sala.getUsuario(usuario.nombreUsuario).color);
        //actualizo su espacio de trabajo
        socket.emit("updateXml", mensaje);
        //le aviso a todos que se conecta un nuevo usuario
        io.to(sala.tokenSala).emit("getUsuarios", sala.usuariosConectados);
        sala.usuariosConectados.forEach((e) => {
          let bloqueSeleccionado: marcaControler = {
            color: e.color,
            idBloque: e.bloqueSeleccionado,
            idMarca: e.idMarcador,
          };
          socket.emit("actualizarBloque", bloqueSeleccionado);
        });
      }
    });

    socket.on("updateXml", (data: blockHandler) => {
      let payload = verify(data.usuario, key);
      const usuario = payload["usuario"];

      const sala: Sala = salas[data.token];
      if (sala) {
        //por si tiene varias sesiones abiertas
        if (!sala.existeUsuario(usuario.nombreUsuario)) {
          sala.agregarUsuario(usuario.nombreUsuario, socket.id);
          io.to(sala.tokenSala).emit("getUsuarios", sala.usuariosConectados);
        }
        socket.emit("getColor", sala.getUsuario(usuario.nombreUsuario).color);
        socket.to(data.token).emit("updateXml", data);
        sala.xml = data.xml;

        sala.usuariosConectados.forEach((e) => {
          let bloqueSeleccionado: marcaControler = {
            color: e.color,
            idBloque: e.bloqueSeleccionado,
            idMarca: e.idMarcador,
          };
          socket.emit("actualizarBloque", bloqueSeleccionado);
        });
      }
    });

    socket.on("getAllSelects", (data: blockHandler) => {
      const sala = salas[data.token];
      if (sala) {
        sala.usuariosConectados.forEach((e) => {
          let bloqueSeleccionado: marcaControler = {
            color: e.color,
            idBloque: e.bloqueSeleccionado,
            idMarca: e.idMarcador,
          };
          socket.emit("actualizarBloque", bloqueSeleccionado);
        });
      }
    });

    socket.on("seleccionarBloque", (data: blockHandler) => {
      let payload = verify(data.usuario, key);
      const usuario = payload["usuario"];

      const sala:Sala = salas[data.token];
      if (sala) {
        if (!sala.existeUsuario(usuario.nombreUsuario)) {
          sala.agregarUsuario(usuario.nombreUsuario, socket.id);
          io.to(sala.tokenSala).emit("getUsuarios", sala.usuariosConectados);
        }
        socket.emit("getColor", sala.getUsuario(usuario.nombreUsuario).color);
        const userSala = sala.agregarBloqueSeleccionado(
          usuario.nombreUsuario,
          data.bloqueMarca,
          data.idMarca
        );
        let bloqueSeleccionado: marcaControler = {
          color: userSala.color,
          idBloque: data.bloqueMarca,
          idMarca: data.idMarca,
        };
        socket.to(data.token).emit("actualizarBloque", bloqueSeleccionado);
      }
    });



    socket.on("disconnect", () => {
      for (const p in salas) {
        salas[p].usuariosConectados = salas[p].usuariosConectados.filter(
          (e) => e.idSocket !== socket.id
        );
        io.to(salas[p].tokenSala).emit(
          "getUsuarios",
          salas[p].usuariosConectados
        );
      }
    });
};

