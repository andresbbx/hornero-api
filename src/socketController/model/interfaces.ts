export interface chatInterface{
    mensaje?:Mensaje,
    usuario?:string,
    token?:string,
}
  
export interface blockHandler {
    token: string;
    xml: string;
    usuario?: string;
    usuariosConectados?: usuarioSala[];
    idMarca?: string;
    bloqueMarca?: string;
}
  
export interface marcaControler {
    idBloque: string;
    idMarca: string;
    color: string;
}
  
export  interface usuarioSala {
    idSocket?: string;
    nombreUsuario?: string;
    color?: string;
    idMarcador?: string;
    bloqueSeleccionado?: string;
}

export interface Mensaje {
    mensaje:string;
    usaurio:usuarioSala;
}


export const EVENTO_CHAT = {
    enviarMensaje:"EnviarMensaje",
    recibirMensaje:"RecibirMensaje"
}
  