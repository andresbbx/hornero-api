import { usuarioSala } from "./interfaces";
import { Chat } from "./Chat";
import { getRandomColor } from "../../functions/utiles";


export class Sala {
    public tokenSala: string;
    public usuariosConectados: usuarioSala[] = [];
    public xml: string;
    public chat:Chat;
  
    constructor(token: string) {
      this.tokenSala = token;
      this.xml = "";
      this.chat = new Chat();
    }
    agregarBloqueSeleccionado(nombre: string, bloque: string, marcaId: string) {
      const usuario = this.usuariosConectados.find(
        (e) => e.nombreUsuario === nombre
      );
      usuario.bloqueSeleccionado = bloque;
      usuario.idMarcador = marcaId;
      return usuario;
    }
    agregarUsuario(nombre: string, idSocket: string): usuarioSala {
      let nuevoUsuario = {
        nombreUsuario: nombre,
        color: getRandomColor(),
        idSocket,
        bloqueSeleccionado: null,
      };
      this.usuariosConectados.push(nuevoUsuario);
      return nuevoUsuario;
    }
    getUsuario(nombre: string) {
      return this.usuariosConectados.find((e) => e.nombreUsuario === nombre);
    }
    existeUsuario(nombre: string) {
      return (
        this.usuariosConectados.filter((e) => e.nombreUsuario === nombre).length >
        0
      );
    }
    eliminarSocket(idSocket: string) {
      this.usuariosConectados.filter((e) => e.idSocket !== idSocket);
    }
  }