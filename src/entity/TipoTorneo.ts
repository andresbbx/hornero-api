import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {Torneo} from "./Torneo";


@Entity("tipotorneo" ,{schema:"hornero" } )
export class TipoTorneo {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"idTipo"
        })
    idTipo:number;
        

    @Column("varchar",{ 
        nullable:false,
        name:"Tipo"
        })
    tipo:string;
        

   
    @OneToMany(()=>Torneo, (torneo: Torneo)=>torneo.tipo,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    torneos:Torneo[];
    
}
