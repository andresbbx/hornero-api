import {BaseEntity,BeforeUpdate,BeforeInsert,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {Rol} from "./Rol";
import {Lenguaje} from "./Lenguaje";
import {CambioClave} from "./CambioClave";
import {Resolucion} from "./Resolucion";
import {TorneoUsuario} from "./TorneoUsuario";
import {hashSync, genSaltSync} from 'bcrypt'
import {saltHash} from '../env'
import { Torneo } from "./Torneo";
import { CodigoUsuario } from './CodigoUsuario'


@Entity("usuario" ,{schema:"hornero" } )
@Index("NombreUsuario",["nombreUsuario",],{unique:true})
@Index("NombreUsuario_2",["nombreUsuario",],{unique:true})
@Index("NombreUsuario_3",["nombreUsuario",],{unique:true})
@Index("NombreUsuario_4",["nombreUsuario",],{unique:true})
@Index("idRol",["rol",])
@Index("idLenguaje",["lenguajeFavorito",])
export class Usuario {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"idUsuario"
        })
    idUsuario:number;

    @Column("varchar",{ 
        nullable:false,
        length:100,
        name:"Institucion"
        })
    institucion:string;
        
    @Column("varchar",{ 
        nullable:false,
        unique: true,
        length:100,
        name:"NombreUsuario"
        })
    nombreUsuario:string;

    @Column("varchar",{ 
        nullable:false,
        length:1000,
        name:"Descripcion"
        })
    descripcion:string;

    @Column("varchar",{ 
        nullable:false,
        length:100,
        name:"Clave",
        select:false
        })
    clave:string;
        
    @ManyToOne(()=>Rol, (rol: Rol)=>rol.usuarios,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'idRol'})
    rol:Rol | null;

    @Column("varchar",{ 
        nullable:false,
        name:"Email"
        })
    email:string;
   
    @ManyToOne(()=>Lenguaje, (lenguaje: Lenguaje)=>lenguaje.usuarios,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'idLenguaje'})
    lenguajeFavorito:Lenguaje | null;
   
    @OneToMany(()=>CambioClave, (cambioclave: CambioClave)=>cambioclave.usuario,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    cambioClaves:CambioClave[];
   
    @OneToMany(()=>Resolucion, (resolucion: Resolucion)=>resolucion.usuario,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    resoluciones:Resolucion[];
    
    @OneToMany(()=> Torneo,(torneosCreados:Torneo)=>torneosCreados.creador,{onDelete:'RESTRICT',onUpdate:"RESTRICT"})    
    torneosCreados:Torneo[]

    @OneToMany(()=>TorneoUsuario, (torneousuario: TorneoUsuario)=>torneousuario.usuario,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    torneosUsuarios:TorneoUsuario[];


    @OneToMany(()=>CodigoUsuario,(codigo:CodigoUsuario)=>codigo.usuario,{ 
        onDelete: 'RESTRICT' ,
        onUpdate: 'RESTRICT' 
    })
    codigosGuardados: CodigoUsuario[]

    @BeforeInsert()
    @BeforeUpdate()
    hashPassword() {
        if (this.clave) {
            const salt = genSaltSync(saltHash);
            this.clave = hashSync(this.clave,salt);
        }
    }

    @BeforeInsert()
    defaultRol() {
        if (!this.rol) {
            this.rol = {idRol:2};
        }
    }
    @BeforeInsert()
    defaultLenguaje() {
        if (!this.lenguajeFavorito) {
            this.lenguajeFavorito = {idLenguaje:1};
        }
    }
    
}
