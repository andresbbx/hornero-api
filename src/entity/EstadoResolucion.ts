import {Column,Entity,OneToMany,PrimaryGeneratedColumn} from "typeorm";
import {Resolucion} from "./Resolucion";


export enum ESTADO_RESOLUCION{
    Iniciado = 1,
    Correcta = 2,
    Incorrecta = 3,
    SuperoTiempoMaximiCorrecta = 4,
    SuperoTiempoMaximiIncorrecta = 5,
    TorneoTerminadoCorrecta = 6,
    TorneoTerminadoIncorrecta = 7,
    TorneoTerminadoSuperaTiempoMaximoCorrecta = 8,
    TorneoTerminadoSuperaTiempoMaximoIncorrecta = 9,
    ProblemaSolucionadoCorrecta = 10,
    ProblemaSolcionadoIncorrecta = 11,
    ProblemaSolucionadoSuperaElTiempoCorrecta = 12,
    ProblemaSolucionadoSuperaElTiempoIncorrecta = 13,
    Penalizacion = 14,
    Correcta1 = 21,
    PuntoGanado = 100
};

@Entity("estadoresolucion" ,{schema: "hornero" } )
export class EstadoResolucion {

    @PrimaryGeneratedColumn({
        type :"int", 
        name :"idEstado"
        })
    idEstado:number;
        

    @Column("varchar",{ 
        nullable :false,
        length :100,
        name :"Estado"
        })
    estado:string;
        

   
    @OneToMany(() =>Resolucion, (resolucion : Resolucion) =>resolucion.estado,{ onDelete : 'RESTRICT' ,onUpdate : 'RESTRICT' })
    resoluciones :Resolucion[];
    
}
