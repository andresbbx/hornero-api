import { Request, Response } from "express";
import { Stub } from "../entity/Stub";
import { getRepository } from "typeorm";


export const getStubs = async (req:Request,res:Response) =>{
    try {
        let select = req.query.select ? JSON.parse((req.query.select) as string ) : ""
        let relations = req.query.relations ? JSON.parse((req.query.relations) as string) : ""
               let result = await getRepository(Stub).find({select:select,relations:relations}) ;
        res.json(result);
      } catch (error) {
        res.status(500).json({ error });
      }
}

export const getStub = async (req:Request,res:Response) => {
    try{
        let id = req.params.id;
        let select = req.query.select ? JSON.parse((req.query.select) as string ) : ""
        let relations = req.query.relations ? JSON.parse((req.query.relations) as string) : ""
              if(Number(id)){
            let result = await getRepository(Stub).findOne({select:select,relations:relations,where:[{idStubs:id}]});
            res.json(result)
        }else{
            res.status(400).json({msg:"Id invalido"})
        }
    }catch(error){
        res.status(500).json(error)
    }
}

export const descargarStub = async (req:Request,res:Response) => {
    try {
        let descarga = req.query.archivo as string;
        let file = `public/stubs/${descarga}`;
        return res.download(file,descarga, err => {
            console.log(err);
        })
    } catch(error) {
        res.status(500).json(error);
    }
}