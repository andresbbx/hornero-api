import { Request, Response } from "express";
import { Rol } from "../entity/Rol";
import { getRepository } from "typeorm";

export const getRoles = async (req: Request, res: Response) => {
    try {
        let select = req.query.select ? JSON.parse((req.query.select) as string ) : ""
        let relations = req.query.relations ? JSON.parse((req.query.relations) as string) : ""
             let result = await getRepository(Rol).find({select:select,relations:relations}) ;
      res.json(result);
    } catch (error) {
      res.status(500).json({ error });
    }
};

export const getRol = async (req:Request,res:Response) => {
    try{
        let id = req.params.id;
        let select = req.query.select ? JSON.parse((req.query.select) as string ) : ""
        let relations = req.query.relations ? JSON.parse((req.query.relations) as string) : ""
              if(Number(id)){
            let result = await getRepository(Rol).findOne({select:select,relations:relations,where:[{idRol:id}]});
            if(result){
                res.json(result)
            }else{
                res.status(400).json({msg:"Id invalido"})
            }
        }else{
            res.status(400).json({msg:"Id invalido"})
        }
    }catch(error){
        res.status(500).json(error)
    }
}

export const createRol = async (req:Request,res:Response) =>{
    res.send({msg:"Not implemented yet"})
}