import {  Request, Response } from "express";
import { Complejidad } from "../entity/Complejidad";
import { getRepository } from "typeorm";


export const getComplejidades = async (req:Request,res:Response) => {
    try {
        let result = await getRepository(Complejidad).find() ;
        res.json(result);
      } catch (error) {
        res.status(500).json({ error });
      }
}