import { Request, Response } from "express";
import { key, expiracionToken } from "../env";
import * as jwt from "jsonwebtoken";
import { compareSync } from "bcrypt";
import { getManager, getRepository } from "typeorm";
import { Usuario as usuarioRepo } from "../entity/Usuario";
import { enviarMail } from "../functions/utiles";


export const recuperarConstrasenia = async (req:Request,res:Response) => {
  try{
      let user = await getRepository(usuarioRepo).findOne({select:["nombreUsuario"],where:{email:req.body.email}})
      
      if(user) {
        let info = await enviarMail(req.body.email,user.nombreUsuario);
        res.json({resultado:"ok",info})
      }else{
        res.status(500).send( {error:"Email  no encontrado"});
       }
  }catch(error){
      console.log(error);
      res.status(500).send(error);
  }
}
export const login = async (req: Request, res: Response) => {
  try {
    let { usuario, pass } = req.body;
    const usuarioConectado = await getRepository(usuarioRepo).findOne({
      relations: ["rol", "torneosUsuarios", "torneosUsuarios.torneo"],
      where: { nombreUsuario: usuario }
    });
    const resp = await getManager()
      .query(`SELECT NombreUsuario, Email, Institucion,Clave, r.idRol,r.Rol
                  FROM usuario u inner join rol r on u.idRol = r.idRol 
                  WHERE NombreUsuario='${usuario}'`);

    if (resp.length === 0 || !compareSync(pass, resp[0].Clave)) {
      res.status(403).json({ msg: "Credenciales incorrectas" });
    } else {
      let fecha = new Date();
      fecha.setHours(fecha.getHours() + 4);
      const usuario: object = {
        idUsuario: usuarioConectado.idUsuario,
        institucion: usuarioConectado.institucion,
        nombreUsuario: usuarioConectado.nombreUsuario,
        descripcion: usuarioConectado.descripcion,
        email: usuarioConectado.email,
        rol: usuarioConectado.rol
      };
      const credenciales = {
        fechaFin: fecha,
        usuario: usuarioConectado,
        token: jwt.sign({ usuario }, key, { expiresIn: expiracionToken })
      };
      res.json(credenciales);
    }
  } catch (error) {
    console.log(error)
    res.status(500).json({ msg: "Ubo un error en el servidor " + error });
  }
};
