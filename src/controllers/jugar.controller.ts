import { Request, Response } from "express";
import { getRepository, Not } from "typeorm";
import { TorneoUsuario } from "../entity/TorneoUsuario";
import { TIPO_TORNEO } from '../entity/Torneo'
import { generateTokenResolucion } from "../functions/utiles";
import { random } from "lodash";
import { Resolucion } from "../entity/Resolucion";
import { ESTADO_TORNEO } from "../entity/EstadoTorneo";
import { ESTADO_RESOLUCION, EstadoResolucion } from "../entity/EstadoResolucion";
import { TorneoProblema } from "../entity/TorneoProblema";
import { Solucion } from "../entity/Solucion";

export const solicitud = async (req: Request, res: Response) => {

	try {
		const fechaActual = new Date().getTime();
		/** token solicitud*/
		const token = req.query.token;
		let problemaOrden: number;

		/** Orden Del Problema*/
		problemaOrden = Number(req.query.problema);
		if (!problemaOrden) {
			throw {
				error: "El tipo de datos de problema tiene que ser Natural",
				status: 400,
			};
		}
		//console.log(problemaOrden);
		//console.log(token)
		let problemasAux = await getRepository<TorneoUsuario>(TorneoUsuario)
			.createQueryBuilder("tu")
			.innerJoinAndSelect("tu.torneo", "torneo")
			.innerJoinAndSelect("tu.usuario", "usuario")
			.innerJoinAndSelect("torneo.tipo", "tipotorneo")
			.innerJoinAndSelect("torneo.torneosProblemas", "torneoproblema")
			.innerJoinAndSelect("torneoproblema.problema", "problema")
			.innerJoinAndSelect("torneo.estado", "estadotorneo")
			.innerJoinAndSelect("problema.soluciones", "solucion")
			.where("torneoproblema.Orden = :orden and tu.Token = :token", {
				orden: problemaOrden,
				token: token,
			})
			.getOne();


		if (!problemasAux) {
			throw {
				error: "El token o numero de problema es incorrecto",
				status: 400,
			};
		}

		if (problemasAux.torneo.estado.idEstado !== ESTADO_TORNEO.EnProceso) {
			throw { error: "El torneo no esta activo", status: 400 };
		}

		/** Obtener ultima resolucion del usuario para saber si esta penalizado */
		let ultimoProblema = await getRepository<Resolucion>(Resolucion).findOne({
			where: {
				usuario: {
					idUsuario: problemasAux.usuario.idUsuario
				}
			},
			order: { fechaRespuesta: "DESC" },
			relations: ['estado']
		});
		//console.log(ultimoProblema.map((e:Resolucion) => {e}));
		// Chequear el usuario no este penalizado
		const calculoTiempo = fechaActual - Number(ultimoProblema.fechaRespuesta);
		const cincoMinutos = (60000 * 1)
		if (ultimoProblema &&
			ultimoProblema.estado.idEstado == ESTADO_RESOLUCION.Incorrecta &&
			calculoTiempo < cincoMinutos) {
			res.json({ status: 2, penalidad: cincoMinutos - calculoTiempo })
			return
		}

		let soluciones =
			problemasAux.torneo.torneosProblemas[0].problema.soluciones;

		if (!soluciones) {
			throw {
				error: "El problema no tiene ninguna resolucion asociado",
				status: 400,
			};
		}

		let solucionElegida = soluciones[random(soluciones.length - 1)];

		soluciones = soluciones.filter(s => s.idSolucion !== solucionElegida.idSolucion)
		delete problemasAux.torneo.torneosProblemas[0].problema.soluciones;

		const resolucionRepo = getRepository<Resolucion>(Resolucion);

		let resolu: Resolucion = resolucionRepo.create({
			torneo: problemasAux.torneo,
			fechaSolicitud: new Date().getTime().toString(),
			estado: { idEstado: ESTADO_RESOLUCION.Iniciado },
			token: await generateTokenResolucion({ problemasAux }),
			usuario: problemasAux.usuario,
			problema: problemasAux.torneo.torneosProblemas[0].problema,
			solucion: solucionElegida,
		});
		let parametrosHornereando;

		/** Si es del tipo hornereando tiene que tener al menos 4 soluciones  */
		if (problemasAux.torneo.tipo.idTipo === TIPO_TORNEO.hornerando) {
			if (soluciones.length >= 3) {
				let incorrectos = [];
				for (let i = 0; i < 3; i++) {
					const solTemp = soluciones[random(soluciones.length - 1)];
					console.log(solTemp.salida)
					incorrectos.push(solTemp.salida);
					soluciones = soluciones.filter(s => s.idSolucion !== solTemp.idSolucion)
				}
				parametrosHornereando = {
					correcta: solucionElegida.salida,
					incorrectos
				}

			} else {
				throw { status: 500, msg: "El problema no tiene suficiente cantidad de resoluciones" }
			}
		}
		await resolucionRepo.save(resolu);

		res.json({
			status: 1,
			parametrosEntrada: solucionElegida.parametrosEntrada,
			token: resolu.token,
			parametrosHornereando
		});
	} catch (error) {
		const status = error.status || 500;
		console.log(error);
		res.status(status).json(error);
	}
};

export const respuesta = async (req: Request, res: Response) => {
	try {
		/**En esta variable agregar 1 menos de la que se necesita */
		let cantidadRespuestasCorrectas = 4;
		const token = req.query.tokenSolicitud;
		const solucion = req.query.solucion as string;
		const tiempoActual: number = new Date().getTime();
		const repositorioResolucion = getRepository<Resolucion>(Resolucion);

		let resolucionRespuesta = await repositorioResolucion
			.createQueryBuilder("r")
			.innerJoinAndSelect("r.torneo", "torneo")
			.innerJoinAndSelect("r.usuario", "usuario")
			.innerJoinAndSelect("r.solucion", "solucion")
			.innerJoinAndSelect("r.problema", "problema")
			.innerJoinAndSelect("r.estado", "estadoresolucion")
			//.leftJoinAndSelect("r.respuesta","respuesta")
			.innerJoinAndSelect("torneo.estado", "tipotorneo")
			.innerJoinAndSelect("torneo.tipo", "estadotorneo")
			.where("r.Token=:token and torneo.idEstado = :estadoTorneo", {
				token,
				estadoTorneo: ESTADO_TORNEO.EnProceso,

			})
			.getOne();
		/** Error de token */
		if (!resolucionRespuesta) {
			throw { error: "El token es invalido", status: 400 };
			/** Resolucion ya respondida */
		} else if (resolucionRespuesta.respuesta) {
			throw { error: "Esta resolucion ya esta respondida", status: 400 };
		} else {
			if(resolucionRespuesta.torneo.tipo.idTipo === TIPO_TORNEO.hornerando){
				cantidadRespuestasCorrectas = 2
			} 
			let nuevoEstado = resolucionRespuesta.estado.idEstado;
			/**Se chequea la correctitud de la respuesta */
			if (resolucionRespuesta.solucion.salida === solucion) {
				nuevoEstado = ESTADO_RESOLUCION.Correcta;
			} else {
				nuevoEstado = ESTADO_RESOLUCION.Incorrecta;
			}


			/**let resolucionesCorrectas = await getRepository(Resolucion).find({
			  relations: ["usuario"],
			  where: {
				estado: {
				  idEstado: ESTADO_RESOLUCION.Correcta,
				},
				usuario: resolucionRespuesta.usuario,
				problema: resolucionRespuesta.problema,
				torneo: resolucionRespuesta.torneo,
			  },
			});*/

			/** Obtengo todas las resoluciones del usuario para el problema ordenado solo los respondidos*/
			const resoluciones = await getRepository(Resolucion).find({
				relations: ["usuario", "estado"],
				where: {
					usuario: resolucionRespuesta.usuario,
					problema: resolucionRespuesta.problema,
					torneo: resolucionRespuesta.torneo,
					estado: { idEstado: Not(ESTADO_RESOLUCION.Iniciado) }
				},
				order: { fechaSolicitud: 'DESC' },
			});

			/** si los ultimos 5 respuestas fueron correctas */
			let mostrar = resoluciones.map(e => {
				return {
					id: e.estado.estado
				}
			})
			//console.log(mostrar)
			let respuestaCorrecta = 0;

			//Arranco el siguiente porque la primera es Resolcion
			if (resoluciones.length >= cantidadRespuestasCorrectas) {
				respuestaCorrecta = resoluciones[0].estado.idEstado === ESTADO_RESOLUCION.Correcta ? 1 : 0;
				for (let i = 1; i < cantidadRespuestasCorrectas; i++) {

					respuestaCorrecta += resoluciones[i].estado.idEstado === ESTADO_RESOLUCION.Correcta ? 1 : 0;
				}
			}
			/**Se chequea que no se haya ganado un punto ya */
			const estaGanado = resoluciones.some(reso => reso.estado.idEstado === ESTADO_RESOLUCION.PuntoGanado);

			/**
			 * TODO: cuando gano 1 punto agregar estampilla de tiempo.
			 * TODO: cuando se penaliza aagregar 1 punto de penalizacion.
			 */
			if ( !estaGanado &&
				respuestaCorrecta === cantidadRespuestasCorrectas &&
				nuevoEstado === ESTADO_RESOLUCION.Correcta) {
					
				nuevoEstado = ESTADO_RESOLUCION.PuntoGanado;
				//obtengo los datos del usuario
				let torneoUsuario = await getRepository(TorneoUsuario).findOne({
					where: {
						usuario: {
							idUsuario: resolucionRespuesta.usuario.idUsuario,
						},
						torneo: {
							idTorneo: resolucionRespuesta.torneo.idTorneo,
						},
					},
				});

				getRepository(TorneoUsuario).merge(torneoUsuario, {
					puntos: torneoUsuario.puntos + 1,
					tiempo: tiempoActual.toString()
				});

				await getRepository(TorneoUsuario).save(torneoUsuario);
			} else if (estaGanado && nuevoEstado === ESTADO_RESOLUCION.Correcta) {
				nuevoEstado = ESTADO_RESOLUCION.ProblemaSolucionadoCorrecta;

			}else if(nuevoEstado === ESTADO_RESOLUCION.Incorrecta) {
				//obtengo los datos del usuario
				let torneoUsuario = await getRepository(TorneoUsuario).findOne({
					where: {
						usuario: {
							idUsuario: resolucionRespuesta.usuario.idUsuario,
						},
						torneo: {
							idTorneo: resolucionRespuesta.torneo.idTorneo,
						},
					},
				});

				let penalidad = torneoUsuario.penalidad ? torneoUsuario.penalidad : 0

				getRepository(TorneoUsuario).merge(torneoUsuario, {
					penalidad: penalidad + 1
				});
				await getRepository(TorneoUsuario).save(torneoUsuario);

			}
			/**Agregar estado nuevo de la solicitud */
			repositorioResolucion.merge(resolucionRespuesta, {
				estado: { idEstado: nuevoEstado },
				respuesta: solucion,
				fechaRespuesta: tiempoActual.toString(),
			});
			const result = await repositorioResolucion.save(resolucionRespuesta);
			const estadoResultado = await getRepository(EstadoResolucion).findOne(
				nuevoEstado
			);
			return res.json(estadoResultado);
		}
	} catch (error) {
		let status = error.status || 500;
		res.status(status).json(error);
		console.log(error);
	}
};

/** Esta funcion es solo habilitada para hornerando */
export const getProblemaActual = async (req: Request, res: Response) => {
	const { token } = req.query
	const usuario = req['usuario'];
	console.log(usuario)
	try {
		const ultimaResolucion = await getRepository(Resolucion).findOne({
			relations: ["problema", "estado", "torneo", "torneo.torneosProblemas", "torneo.torneosProblemas.problema"],
			where: { token },
			order: { fechaRespuesta: "DESC" }
		})
		if (!ultimaResolucion) {
			console.log("orden ")
			res.json({ problemaActual: 1 })
		} else {

			const problemaSolucion = ultimaResolucion.torneo.torneosProblemas.find(e => {
				return e.problema.idProblema === ultimaResolucion.problema.idProblema
			})
			let orden = problemaSolucion.orden;
			if (ultimaResolucion.estado.idEstado === ESTADO_RESOLUCION.PuntoGanado) {
				orden++
			}
			res.json({ problemaActual: orden })
		}
	} catch (error) {
		const status = error.status || 500
		res.status(status).json(error)
		console.log(error)
	}

}
