const jwt = require('jsonwebtoken');
import { Request, Response,NextFunction } from "express";
import { key } from '../env'
import { ROL } from "../entity/Rol";



export const auth = (req: Request, res: Response, next: NextFunction)=>{
    try{
        const token = req.get("token");
        req['usuario'] = jwt.verify(token,key).usuario;
        next();
    }catch(err){
        return res.status(401).json({
            error:err,
            ok:false
        })
    }
}


export const isAdmin = (req:Request,res:Response,next:NextFunction) =>{
    try{
        const token = req.get("token");
        const payload = jwt.verify(token,key);;
        req['usuario'] = payload.usuario;
        if(payload.usuario.rol.idRol !== ROL.admin){
            throw {msg:"Necesita ser administrador para acceder a estas funciones"}
        }else{
            next();
        }

    }catch(err){
        return res.status(401).json({
            error:err,
            ok:false
        })
    }
}


export const isDocente = (req:Request,res:Response,next:NextFunction) =>{
    try{
        const token = req.get("token");
        const usuario = jwt.verify(token,key);;
        req['usuario'] = usuario.usuario;
        if(usuario.usuario.rol.idRol !== ROL.docente && usuario.usuario.rol.idRol !== ROL.admin){
            throw {msg:"Necesita ser docente para acceder a estas funciones"}
        }else{
            next();
        }

    }catch(err){
        return res.status(401).json({
            error:err,
            ok:false
        })
    }
}

