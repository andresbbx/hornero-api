import { Router } from "express";
import { getTorneo,getTorneos, createTorneo,editTorneo, registrarTorneo} from "../controllers/torneos.controller"
import {auth, isDocente} from "../middleware/auth";

const router = Router();

router.get("/",getTorneos);
router.get("/:id",getTorneo);
router.post("/",[isDocente],createTorneo);
router.put("/:id",[isDocente],editTorneo);
router.post("/inscribir/:id",[auth],registrarTorneo)

export = router;
