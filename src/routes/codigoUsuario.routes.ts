import { Router } from "express";
import {createCodigo} from "../controllers/codigoUsuario.controller";
import {auth} from "../middleware/auth"
const router = Router();

router.post("/",[auth],createCodigo);
export = router