import { Router } from "express";
import { getStubs,getStub,descargarStub } from "../controllers/stubs.controller";

const router = Router();

router.get("/", getStubs);
router.get('/descargar',descargarStub);
router.get("/:id", getStub);

export = router;
