import { Router } from "express";
import {getLenguaje,getLenguajes,createLenguaje} from "../controllers/lenguaje.controller"
import { isAdmin } from '../middleware/auth'
const router = Router();

router.get("/", getLenguajes);
router.get("/:id",getLenguaje);
router.post("/",[isAdmin],createLenguaje);
/*
router.put("/",editLenguaje);
*/

export = router;
