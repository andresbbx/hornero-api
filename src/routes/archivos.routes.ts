import { Router, Request, Response } from "express";


import * as multer from 'multer'
import {join, relative} from "path"
const router = Router();

const relativePath = join(__dirname,"../../public");
var upload = multer({ 
    storage:multer.diskStorage({
        destination:(req,file,callback) =>{
            let tipo  = req.body.tipo;
            let path = join(relativePath,tipo)
            callback(null,path)
        },
        filename: (req,file,callback) =>{
            let nombre = req.body.nombre
            callback(null, nombre);
        }
    })
 })

router.post('/',upload.single('file'), async (req:Request,res:Response) => {
    res.status(200).send({status:"okey"});
})
export =  router;
