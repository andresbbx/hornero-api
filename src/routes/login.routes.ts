import { Router } from "express";
import {login, recuperarConstrasenia} from "../controllers/login.cotroller";
const router = Router();

router.post("/", login);
router.post("/recuperar-pass",recuperarConstrasenia)

export = router;
