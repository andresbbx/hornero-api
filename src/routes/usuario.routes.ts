import { Router} from "express";
import {getUsuario,getUsuarios,emailRegistrado,createUsuario,usuarioRegistrado, editPassword} from '../controllers/usuario.controller'
import { isAdmin } from "../middleware/auth";

const router = Router();


router.get("/",[isAdmin],getUsuarios);
router.post('/emailRegistrado',emailRegistrado);
router.put('/resetear-password',editPassword)
router.get("/:id",getUsuario);
router.post('/',createUsuario);

router.post('/nombreRegistrado',usuarioRegistrado)


export = router;