import { Router } from "express";
import {solicitud,respuesta,getProblemaActual} from "../controllers/jugar.controller";
import { auth } from "../middleware/auth";

const router = Router();

router.get("/solicitud", solicitud);
router.get("/respuesta", respuesta);
router.get("/ultimoProblema",[auth],getProblemaActual);



export = router;
