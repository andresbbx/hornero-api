import express = require("express");
import * as server from 'http'
import io = require('socket.io');
import path = require('path')
export default class Server {
  public app: express.Application;
  public port: number;
  public server:any;
  public socketIo:io.Server;

  constructor(port: number) {
    this.port = port;
    this.app = express();
    this.server = server.createServer(this.app);
    this.app.use("/api",express.static(path.join(__dirname, '../../public')));

    this.socketIo = io(this.server);
  }

  static init(port: number) {
    return new Server(port);
  }

  start(callback: () => void) {
    this.server.listen(this.port, callback);
  }
}
