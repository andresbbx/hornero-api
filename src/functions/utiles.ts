import { Torneo } from "../entity/Torneo";
import { Usuario } from "../entity/Usuario";
import { genSaltSync, hashSync } from "bcrypt";
import { saltHash } from "../env";
import { TorneoUsuario } from "../entity/TorneoUsuario";
import { getRepository } from "typeorm";
import { Resolucion } from "../entity/Resolucion";
import * as nodemailer from "nodemailer";
import { key, host } from "../env";
import * as jwt from "jsonwebtoken";

export const generateToken = async (torneo: Torneo, usuario: Usuario) => {
	try {
		let hashEncontrado = false;
		let hash: string;
		do {
			let salt = genSaltSync(saltHash);
			hash = hashSync(JSON.stringify({ ...torneo, ...usuario }), salt);
			hash = hash.substring(0, 31)
			let buscarHash = await getRepository(TorneoUsuario).findOne({
				where: { token: hash }
			});
			if (!buscarHash) {
				hashEncontrado = true;
			}
		} while (!hashEncontrado);
		return hash;
	} catch (error) {
		throw error;
	}
};

export const generateTokenResolucion = async (data: any) => {
	try {
		let hashEncontrado = false;
		let hash: string;
		do {
			let salt = genSaltSync(saltHash);
			hash = hashSync(JSON.stringify({ ...data }), salt);
			hash = hash.substring(0, 31);
			let buscarHash = await getRepository(Resolucion).findOne({
				where: { token: hash }
			});
			if (!buscarHash) {
				hashEncontrado = true;
			}
		} while (!hashEncontrado)
		return hash;
	} catch (error) {
		throw error;
	}
}

export const enviarMail = async (email: string, usuario: string = '') => {
	try {
		let hash = jwt.sign({ email }, key, { expiresIn: '1 h' });
		// create reusable transporter object using the default SMTP transport
		let transporter = nodemailer.createTransport({
			host: "smtp.gmail.com",
			port: 587,
			secure: false, // true for 465, false for other ports
			auth: {
				user: 'hornero@fi.uncoma.edu.ar', // generated ethereal user
				pass: "hornero@123", // generated ethereal password
			},
		});

		// send mail with defined transport object
		let info = await transporter.sendMail({
			from: '"Honero:Zorzal" <hornero@fi.uncoma.edu.ar>', // sender address
			to: email, // list of receivers
			subject: `Recupero de Constrasenia para usuario: ${usuario}`, // Subject line
			html: `Para recuperar su contrasenia ingrese a 
                  <a href="${host}/#/resetear-password/${hash}">Aqui!</a>`, // html body
		});

		return info;
	} catch (error) {
		throw error
	}
}

export function getRandomColor() {
	var letters = '0123456789ABCDEF';
	var color = '#';
	for (var i = 0; i < 6; i++) {
		color += letters[Math.floor(Math.random() * 16)];
	}
	return color;
}

