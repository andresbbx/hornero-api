import { ESTADO_TORNEO,EstadoTorneo } from '../entity/EstadoTorneo';
import { Torneo } from '../entity/Torneo';
import {getConnection  } from "typeorm";


//Funcion que actualiza el estado de los torneos 
export const cambiarEstadoTorneo = async () => {
    try{
        const fechaActual:Date = new Date();
        
        console.log("se ejecuto el cron")
        //Si el estado de inicio es mas chico que el dia de hoy pasa el torneo a EnProceso
        await getConnection().
            createQueryBuilder().
            update(Torneo).
            set({estado: {idEstado: ESTADO_TORNEO.EnProceso}}).
            where("FechaInicio < :fechaActual", {fechaActual}).
            execute();

        //Si el estado de fin es mas chico que el dia de hoy pasa el torneo a Finalizado
        await getConnection().
            createQueryBuilder().
            update(Torneo).
            set({estado: {idEstado: ESTADO_TORNEO.Terminado}}).
            where("FechaFin < :fechaActual", {fechaActual}).
            execute();
        
        
    }catch(error){
        console.log(new Date() + ": Error a Actualizar los estados de los torneos \n " + error)
    }
}